package com.codistan.loopexercises;

import java.util.Scanner;

public class LoopScenario4 {

	public static void main(String[] args) {

		/*
		 * An employee earns $30/hour for the weekdays. And $40/hour for the Saturday.
		 * He works 8 hours on the weekdays except Fridays. 
		 * Friday and Saturday are optional. And working hours are; 6 hours for Friday and 4 hours for Saturday.
		 * Ask the employee how many times he worked on Friday in this month?
		 * And ask how many times he worked on Saturday?
		 * Find out his earnings for this month and print it out to the console.
		*/
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("How many Fridays did you work in this month: ");
		int frWorkingDay = myScanner.nextInt();
		
		System.out.print("How many Saturdays did you work in this month: ");
		int satWorkingDay = myScanner.nextInt();
		
		int frWorkingHours = 0;
		int satWorkingHours = 0;
		int totMonthlyEarning = 0;
		
		switch (frWorkingDay) {
		case 1:
			frWorkingHours=6;
			break;
		case 2:
			frWorkingHours=12;
			break;
		case 3:
			frWorkingHours=18;
			break;
		case 4:
			frWorkingHours=24;
			break;
		default:
			break;
		}
		
		switch (satWorkingDay) {
		case 1:
			satWorkingHours=4;
			break;
		case 2:
			satWorkingHours=8;
			break;
		case 3:
			satWorkingHours=12;
			break;
		case 4:
			satWorkingHours=16;
			break;

		default:
			break;
		}
		
		totMonthlyEarning=(16*8*30)+(frWorkingHours*30)+(satWorkingHours*40);
		System.out.println("You earned " + "$" + totMonthlyEarning + " this month");
		
	}
}
