package com.codistan.loopexercises;

import java.util.Scanner;

public class LoopScenario1 {

	public static void main(String[] args) {

		/* 
		 * Ask user to give a number (which is divisible by 3) between 1-20. 
		 * Then print out the word of that number to the console.  
		 * (if 3 is entered, Print out "You typed Three")
		 * Calculate the sum of all numbers (which is divisible by 3) up to the given number.
		 * If the user types a number below 1, over 20 or something else, Ask again. 
		*/
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Please enter a number between 1-20");
		int number = myScanner.nextInt();
		
		while (number<1 || number>20 || !(number%3==0)) {
			System.out.println("Please enter a valid number between 1-20 which is divisible by 3");
			number = myScanner.nextInt();
		} 
		
		if(number%3==0)
		{
			switch (number) {
			case 3:
				System.out.println("You typed 'Three'");
				break;
			case 6:
				System.out.println("You typed 'Six'");
				break;
			case 9:
				System.out.println("You typed 'Nine'");
				break;
			case 12:
				System.out.println("You typed 'Twelve'");
				break;
			case 15:
				System.out.println("You typed 'Fifteen'");
				break;
			case 18:
				System.out.println("You typed 'Eighteen'");
				break;
			default:
				break;
			}
			
				int top = 0;
				for (int j = 1; j < number; j++) {
					if(j%3==0)
					{
						top=top+j;
					}
				}
				System.out.println("The sum of all the numbers: " + top);
				
		}
		
		
		
	}
}
