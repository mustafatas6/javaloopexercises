package com.codistan.loopexercises;

import java.util.Scanner;

public class LoopScenario2 {

	public static void main(String[] args) {

		/*
		 * Ask user to type a word.
		 * Get the character size of that word,
		 * Multiply by 5 then print out the result.
		 * Then print out the sum of all odd numbers up to the result.
		*/
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Please type a word");
		String word = myScanner.nextLine().trim().toLowerCase();
		
		int charSize = word.length();
		int result = charSize*5;
		System.out.println(result);
		
		int top=0;
		for (int i = 0; i < result; i++) {
			if(i%2==1)
			{
				top=top+i;
			}
		}
		System.out.print("The sum of the odd numbers between 0-" + result + " is: " + top);
		
		
	}
}
