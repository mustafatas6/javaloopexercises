package com.codistan.loopexercises;

import java.util.Scanner;

public class LoopScenario5 {

	public static void main(String[] args) {

		/*
		 * A student took 3 exams for Math. Ask him to enter his exam results.
		 * Calculate the average of the results.
		 * Tell him his Grade. (A=85-100 / B=70-85 / C=55-70 / D=40-55 / F=0-40)
		 * If his grade is F, give a message that says "You failed".
		 * otherwise, give a message that says "You passed"
		*/

		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Please enter the results of your 3 exams");
		int exam1 = myScanner.nextInt();
		int exam2 = myScanner.nextInt();
		int exam3 = myScanner.nextInt();
		
		int avg = (exam1+exam2+exam3)/3;
		
		if (exam1<0 || exam2<0 || exam3<0 || exam1>100 || exam2>100 || exam3>100) {
			System.out.println("You entered invalid exam result(s)");
		}
		else {
		
		if (avg>85) {
			System.out.println("Your Grade is 'A'");
		}
		else if (avg>70) {
			System.out.println("Your Grade is 'B'");
		}
		else if (avg>55) {
			System.out.println("Your Grade is 'C'");
		}
		else if (avg>40) {
			System.out.println("Your Grade is 'D'");
		}
		else if (avg<=40) {
			System.out.println("Your Grade is 'F'");
		}
		
		if(avg<=40) { 
			System.out.println("You failed");}
		
		else {
			System.out.println("You passed");
		}
		
		}
		
		
	}

}
