package com.codistan.loopexercises;

import java.util.Scanner;

public class LoopScenario3 {

	public static void main(String[] args) {

		/*
		 * Ask user to enter 3 different words which the character numbers are less than 10.
		 * Find out the shortest word, reverse it and print out to the console.
		 * If the character numbers are same for 3 words then Give a message "Character Lengths are equal!".
		 * If any of the given words has more characters than 10, Give a message "You exceeded the length".
		*/
		
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Please type 3 words up to 10 character");
		String word1 = myScanner.nextLine().trim().toLowerCase();
		String word2 = myScanner.nextLine().trim().toLowerCase();
		String word3 = myScanner.nextLine().trim().toLowerCase();
		
		int word1Len = word1.length();
		int word2Len = word2.length();
		int word3Len = word3.length();
		
		StringBuilder revWord1 = new StringBuilder(word1);
		StringBuilder revWord2 = new StringBuilder(word2);
		StringBuilder revWord3 = new StringBuilder(word3);
		
		if (word1Len>10 || word2Len>10 || word3Len>10) {
				System.out.println("You exceeded the length !!!");
			}

		else if (word1Len==word2Len && word1Len==word3Len) {
			System.out.println("Character Lengths are equal!");
		}
		else if (word1Len<word2Len && word1Len<word3Len) {
			
			System.out.println("The reverse of the shortest word you typed is: " + revWord1.reverse());
		}
		else if (word2Len<word1Len && word2Len<word3Len) {
			
			System.out.println("The reverse of the shortest word you typed is: " + revWord2.reverse());
		}
		else {
			
			System.out.println("The reverse of the shortest word you typed is: " + revWord3.reverse());
		}	
	
		
	}
}
